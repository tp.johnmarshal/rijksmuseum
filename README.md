# Albert Heijn - API Automation (Rijksmuseum)

### Prerequisites

- Git
- An IDE

### External Libraries Used

- Rest Assured
- lombok 4.0.0
- Serenity BDD 2.6.0
- Cucumber 2.6.0
- Gradle 6.8
- JDK 15 or below

## Code changes done on following:
* Add/removal of directories to arrange the code as per the purpose
* Report generation
* Application configured to run on multiple environments by creating respective run configurations
* DSL implementation

## Running the tests

1.Use below command to execute the tests:

From IDE:
  * Clone the code into an IDE
  * Build the project
  * Create a run configuration like below and run
      **clean test aggregate reports -Denv=staging -DtargetURL='https://www.rijksmuseum.nl/'**

    <img width="1037" alt="image" src="https://github.com/tpjohnmarshal/rijksmuseum/assets/72710010/928096e1-ccba-401f-a035-0025734963df">

Linux/Mac:
```
./gradlew test \
-Denv=<ENVIRONMENT (local or staging)> \
-DtargetURL='https://waarkoop-server.herokuapp.com'
```

or to run the whole build
```
./gradlew build \
-Denv=<ENVIRONMENT (local or staging)> \
-DtargetURL='https://www.rijksmuseum.nl/'
```
> On windows, use `gradle.bat`

2.If all works fine you will find an index.html file inside [**target/site/serenity**](/target/site/serenity) folder with the results of the
tests.

## Example Report:

<img width="1728" alt="image" src="https://github.com/tpjohnmarshal/rijksmuseum/assets/72710010/26424c9f-6b3f-47e4-997f-8288ff686cca">

<img width="1728" alt="image" src="https://github.com/tpjohnmarshal/rijksmuseum/assets/72710010/e0ebd822-258c-47f5-b64e-675e798d1611">


# Architecture

The architectural approach is based on Dave Farley's DSL + Protocol Drivers approach.

### Why this Architecture?

* Easy maintenance
* Data abstraction between test cases, step definitions, implementation and validation layers
* Each CURD operation needs only one implementation irrespective of number of test scenarios

The project is divided into 4 layers:

**1 - Test Cases:**

Placed at the folder **src/test/resources/features/**, the test cases are scenarios written in Gherkin.
Their goal is to describe the capabilities of the System Under Test (SUT), so that their successful
verification would mean that the SUT has the behaviors we want it to have.


**2 - Domain Specific Language (DSL):**

Placed at the folder **src/test/java/dsl**, the goal this layer is two-fold:

1 - Describe the flows of behavior to implement each step defined in the Test Cases, on the _usecases_ folders.
As a side effect of the definition of these flows, data-structures named Entities will be used created in order to
package the data necessary to interact with the underlying systems and check their responses.

2 - Define the behavior of the systems the project will interact, named _XYZProtocol_.


**3 - Protocol Drivers (PD):**

Placed at the folder **src/test/java/protocoldrivers** folder, PDs implement a _Protocol_ interface, against
a particular implementation of the SUT and other systems.


**4 - System Under Test:**

The SUTs are external to this project. Here, we target the https://data.rijksmuseum.nl/ to search product availability.

## FAQ

#### How can I get the project into my IDE?
Clone or download the project repository from the following GitHub link. https://github.com/tpjohnmarshal/rijksmuseum.git

#### How can I execute the tests?
Refer the **Running the tests** section or inside your IDE create a gradle run configurations and provide the -Denv and -DtargetURL values and run the tests.

#### I'm getting compile errors. What is the next step?
Make sure you have downloaded/cloned the project correctly. Also verify the compiler settings in your IDE.

#### Do I need any proxy settings to run the tests?
NO.
