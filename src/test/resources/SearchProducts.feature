
  @REQ_RD_SEARCH
  Feature: Validate Object Metadata API

  @TEST_RD_1001 @automated
  Scenario: User is allowed to search an object by using Object Number
    Given the user search for an object using a valid object number
    Then the search is successful
    And a details of the object number is retrieved


  @TEST_RD_1002 @automated
  Scenario: Notify the user when the provided credentials are not valid to fetch the object details
    Given the customer search for an object with invalid credentials
    Then the search is unsuccessful

  @TEST_RD_1002 @automated
  Scenario: User is allowed to search an object of a specific maker
    Given the customer search the objects from a specific maker
    Then the search is successful
    And the customer see the objects of the given provider