package dsl.response.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ObjectResponse {
    Integer elapsedMilliseconds;
    ArtObject artObject;
    ArtObject[] artObjects;
    ArtObjectPage artObjectPage;
}
