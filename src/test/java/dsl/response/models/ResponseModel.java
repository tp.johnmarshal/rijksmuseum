package dsl.response.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Value
@Builder
@Getter
public class ResponseModel {
    Integer statusCode;
    ObjectResponse objectResponse;
}
