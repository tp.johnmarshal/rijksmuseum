package dsl.apioperations.usecases.search;

import dsl.apioperations.ApiOperationsFactory;
import dsl.apioperations.ApiOperationsProtocol;
import dsl.apioperations.entities.InputForApiOperations;
import dsl.apioperations.entities.Object;
import dsl.apioperations.entities.Provider;
import dsl.apioperations.entities.StatusCodes;
import dsl.response.models.ArtObject;
import dsl.response.models.ResponseModel;
import lombok.val;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;

public class SearchProduct {
    public static ResponseModel responseModel;
    transient InputForApiOperations inputForApiOperations;
    transient ApiOperationsProtocol apiOperations = ApiOperationsFactory.getProtocol();

    public void searchProduct(){
        inputForApiOperations = InputForApiOperations.builder()
                .objectNumber(Object.VALID_OBJECT.getValue())
                .build();
        responseModel = apiOperations.searchObject(inputForApiOperations);
    }

    public void validateSuccessfulProductSearchStatus(){
        Assert.assertEquals("Product search not returned expected status code", StatusCodes.SUCCESS.getValue(),
                                                                                        responseModel.getStatusCode());
    }

    public void validateObjectNumber() {
        Assert.assertEquals("Response not contains the expected objectNumber",
                                        Object.VALID_OBJECT.getValue(), responseModel.getObjectResponse()
                        .getArtObject().getObjectNumber());
    }

    public void searchProductWithInvalidCredentials() {
        inputForApiOperations = InputForApiOperations.builder()
                .objectNumber(Object.VALID_OBJECT.getValue())
                .build();
        responseModel = apiOperations.searchObjectWithInvalidCredentials(inputForApiOperations);
    }

    public void validateUnauthorizedError() {
        Assert.assertEquals("User entered wrong credentials",
                StatusCodes.UNAUTHORIZED.getValue(), responseModel.getStatusCode());
    }

    public void searchProductFromSpecificMaker() {
        inputForApiOperations = InputForApiOperations.builder()
                .providerName(Provider.VALID_PROVIDER.getValue())
                .build();
        responseModel = apiOperations.searchObject(inputForApiOperations);
    }

    public void validateObjectsFromSpecificProvider() {
        ArtObject[] artObjectList = responseModel.getObjectResponse().getArtObjects();
        val softly = new SoftAssertions();
        for (ArtObject artObject : artObjectList) {
            validateProviderName(artObject, softly);
        }
        softly.assertAll();
    }

    public static void validateProviderName(ArtObject artObject, SoftAssertions softly) {
        softly.assertThat(artObject.getPrincipalOrFirstMaker())
                .withFailMessage("Products returned from invalid maker. " +
                        "Principal maker name : " + artObject.getPrincipalOrFirstMaker())
                .isEqualTo(Provider.VALID_PROVIDER.getValue());
    }
}
