package dsl.apioperations.usecases.search;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class SearchProductFlow {
    private final transient SearchProduct searchProduct = new SearchProduct();

    @Given("the user search for an object using a valid object number")
    public void theUserSearchForAnObjectUsingAValidObjectNumber() {
        searchProduct.searchProduct();
    }

    @Then("the search is successful")
    public void theSearchIsSuccessful() {
        searchProduct.validateSuccessfulProductSearchStatus();
    }

    @And("a details of the object number is retrieved")
    public void aDetailsOfTheObjectNumberIsRetrieved() {
        searchProduct.validateObjectNumber();
    }

    @Given("the customer search for an object with invalid credentials")
    public void theCustomerSearchForAnObjectWithInvalidCredentials() {
        searchProduct.searchProductWithInvalidCredentials();
    }

    @Then("the search is unsuccessful")
    public void theSearchIsUnsuccessful() {
        searchProduct.validateUnauthorizedError();
    }

    @Given("the customer search the objects from a specific maker")
    public void theCustomerSearchTheObjectsFromASpecificMaker() {
        searchProduct.searchProductFromSpecificMaker();
    }

    @And("the customer see the objects of the given provider")
    public void theCustomerSeeTheObjectsOfTheGivenProvider() {
        searchProduct.validateObjectsFromSpecificProvider();
    }
}
