package dsl.apioperations.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Provider {
    VALID_PROVIDER("Rembrandt van Rijn");

    @Getter
    private final String value;
}
