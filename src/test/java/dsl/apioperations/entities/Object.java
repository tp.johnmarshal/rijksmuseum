package dsl.apioperations.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Object {
    VALID_OBJECT("SK-C-5");

    @Getter
    private final String value;
}
