package dsl.apioperations.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum StatusCodes {
    SUCCESS(200),
    UNAUTHORIZED(401);

    @Getter
    private final Integer value;
}
