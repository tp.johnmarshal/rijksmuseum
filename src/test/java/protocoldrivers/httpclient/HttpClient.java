package protocoldrivers.httpclient;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import lombok.extern.java.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import static net.serenitybdd.rest.SerenityRest.rest;

@Log
public class HttpClient {
    private final transient String uri;
    private transient RequestSpecification request;


    public HttpClient(String uri) {
        this.uri = uri;
        request = initRequest(this.uri);
    }

    public RequestSpecification initRequest(String uri) {
        return rest().baseUri(uri).given().relaxedHTTPSValidation();
    }


    public Response call(Method method, String endpoint) {
        log.info(String.format("Calling: %s %s", method, endpoint));
        Response response = executeCall(method, endpoint);
        return handleResponse(response);
    }

    private Response executeCall(Method method, String endpoint) {
        switch (method) {
            case GET:
                return handleResponse(request.get(endpoint));
            case POST:
                return handleResponse(request.post(endpoint));
            case PATCH:
                return handleResponse(request.patch(endpoint));
            case PUT:
                return handleResponse(request.put(endpoint));
            case DELETE:
                return handleResponse(request.delete(endpoint));
            default:
                throw new IllegalArgumentException("You tried to call a non-existing HTTP method");
        }
    }

    public Response handleResponse(Response response) {
        log.info(
                String.format("Response with status %d and body %s", response.statusCode(), response.body().prettyPrint())
        );
        return response;
    }

    public void setApiKey() {
        request = initRequest(this.uri);
        request.queryParam("key", getApiKey());
    }

    public void setQueryParams(String involvedMaker) {
        request = initRequest(this.uri);
        request.queryParam("key", getApiKey());
        request.queryParam("involvedMaker", involvedMaker);
    }

    public void setInvalidApiKey() {
        request = initRequest(this.uri);
        request.queryParam("key", "WRONG-KEY");
    }

    private String getApiKey() {
        Properties properties = new Properties();
        BufferedInputStream stream;
        try {
            stream = new BufferedInputStream(Files.newInputStream(Paths.get("src/main/resources/application-dev.properties")));
            properties.load(stream);
            String apiKey = properties.getProperty("APP_KEY");
            stream.close();
            return apiKey;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
