package protocoldrivers.apioperations.extractors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dsl.response.models.ObjectResponse;
import io.restassured.response.Response;

public class ResponseExtractor {
    ObjectMapper objectMapper = new ObjectMapper();
    public Integer extractStatusCode(Response response) {
        return response.getStatusCode();
    }

    public ObjectResponse extractObjectResponse(Response response) {
        try {
           return objectMapper.readValue(response.getBody().asString(), ObjectResponse.class);
        } catch (NullPointerException | JsonProcessingException e) {
            return null;
        }
    }
}
