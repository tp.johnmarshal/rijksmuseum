package protocoldrivers.apioperations;

import dsl.apioperations.ApiOperationsProtocol;
import dsl.apioperations.entities.InputForApiOperations;
import dsl.configuration.ConfigurationFactory;
import dsl.response.models.ResponseModel;
import io.restassured.http.Method;
import io.restassured.response.Response;
import protocoldrivers.apioperations.modelers.ResponseModeler;
import protocoldrivers.httpclient.HttpClient;

public class ApiOperations implements ApiOperationsProtocol {
    transient ResponseModeler responseModeler;
    transient HttpClient client;
    static String searchEndpoint = "api/nl/collection";

    public ApiOperations(ConfigurationFactory.AutomationURLs automationURLs){
        client = new HttpClient(automationURLs.getRijksMuseumURL());
        responseModeler = new ResponseModeler();
    }

    @Override
    public ResponseModel searchObject(InputForApiOperations inputForApiOperations) {
        Response response = prepareAndCallSearchProduct(inputForApiOperations);
        return responseModeler.prepareResponseModelForSearchProduct(response);
    }

    private Response prepareAndCallSearchProduct(InputForApiOperations inputForApiOperations) {
        if (inputForApiOperations.getObjectNumber() != null) {
            client.setApiKey();
            return client.call(Method.GET, createSearchProductEndpoint(inputForApiOperations));
        } else {
            client.setQueryParams(inputForApiOperations.getProviderName());
            return client.call(Method.GET, searchEndpoint);
        }
    }

    private String createSearchProductEndpoint(InputForApiOperations inputForApiOperations) {
        return String.format("%s/%s", searchEndpoint, inputForApiOperations.getObjectNumber());
    }

    @Override
    public ResponseModel searchObjectWithInvalidCredentials(InputForApiOperations inputForApiOperations) {
        client.setInvalidApiKey();
        Response response = client.call(Method.GET, createSearchProductEndpoint(inputForApiOperations));
        return responseModeler.prepareResponseModelForSearchProduct(response);
    }
}
